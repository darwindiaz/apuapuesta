<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApuestaModel extends Model
{
    //

    protected $table = 'apuestas';
	
    protected $fillable = [
		'nombre_apuesta',
		'valor_apuesta',
		'aversario1_apuesta',
		'aversario2_apuesta',
		'tipo_apuesta',
		'fin_apuesta'
		];
}
