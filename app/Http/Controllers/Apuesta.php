<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\ApostadoresModel;
use App\ApuestaModel;
use App\Http\Requests\CreateApuestaRequest;
use Illuminate\Support\Facades\Session;

class Apuesta extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $apuestas = ApuestaModel::all();
        return view('misapuestas',compact('apuestas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('nuevaapuesta');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateApuestaRequest $request)
    {
       
        //dd($request->all());
        //Session::put('apuestaCreada',true);
        //Session::put('apostadorCreado',false);

        $apuesta = ApuestaModel::create($request->all());
        
        $json = [
            'id'=>$apuesta->id,
            ];
        return response()->json($json);
        //return view('nuevaapuesta',compact('apuesta'));

       // return redirect()->to('apuesta/create',compact($apuesta));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $apuesta = ApuestaModel::findOrFail($id);
        return view('nuevaapuesta',compact('apuesta'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $apuesta = ApuestaModel::findOrFail($id);

        $apostadores = ApostadoresModel::where('id_apuesta',$apuesta->id)->get();
        return view('editarapuesta',compact('apuesta','apostadores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateApuestaRequest $request, $id)
    {
        
        $apuesta = ApuestaModel::findOrFail($id);
        $apuesta->fill($request->all());
        $apuesta->save();
        
        return response()->json('true');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $apuesta = ApuestaModel::findOrFail($id);
        $apuesta->delete();
        return response()->json('true');
    }
}
