<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ApostadoresModel;
use App\ApuestaModel;
use App\Http\Requests\CreateApostadoresRequest;
use Illuminate\Support\Facades\Session;


class Apostadores extends Controller
{


     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateApostadoresRequest $request)
    {

       // Session::put('apostadorCreado',true);

        $apostador = ApostadoresModel::create($request->all());
        return response()->json($apostador);      
        //$apuesta=ApuestaModel::findOrFail($request->id_apuesta);
        //$apostadores = ApostadoresModel::where('id_apuesta',$request->id_apuesta)->get();
        
        //return redirect()->to('apuesta/create')->with( 'apuesta', $apuesta )->with( 'apostadores', $apostadores );
        
        //Capturar Pantalla
        /*
        $im = imagegrabscreen();
        imagepng($im, $request->id_apuesta."apuesta.png");
        imagedestroy($im);
        */
        //return view('nuevaapuesta',compact('apuesta','apostadores'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $apuesta=ApuestaModel::findOrFail($id);
        return view('compartir',compact('apuesta'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $apostador = ApostadoresModel::findOrFail($id);

        return response()->json($apostador);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateApostadoresRequest $request, $id)
    {
        $apostador = ApostadoresModel::findOrFail($id);
        $apostador->fill($request->all());
        $apostador->save();
        
        return response()->json($apostador);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $apostador = ApostadoresModel::findOrFail($id);
        $apostador->delete();
        return response()->json('true');
    }
}
