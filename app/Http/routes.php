<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/', ['middleware' => 'auth', function () {
    return view('home');
}]);

/*
Route::get('/nueva-apuesta', ['middleware' => 'auth', function () {
    return view('nuevaapuesta');
    
}]);
*/

/* 	con el siguiente comando se crea el controlador con dotos sus medotos para el tipico CRUD
	php artisan make:controller Apuesta
	Podemos revizar en la capeta ontroller 
	tambien podemos ver todas las rutas quie tiene hasta el momento nuestra aplicacion con:
	php artisan route:list
	hasta el momento no esta la rutas para el nuevo controlador, con locando la siguuente linea en
	el archivo de route.php
	Route::resource('apuesta', 'Apuesta');
	apuesta -> Nombre de la ruta
	Apuesta -> Nombre del controlador
	ahora si vemos otravez las ruta ya estan las nuevas.
	php artisan route:list

	Alternativa Route::resource('apuesta', 'Apuesta', ['only' => [
	    'index', 'show'
	]]);

	Route::resource('apuesta', 'Apuesta', ['except' => [
	    'create', 'store', 'update', 'destroy'
	]]);
*/


Route::resource('apuesta', 'Apuesta');
Route::resource('apostadores', 'Apostadores');

