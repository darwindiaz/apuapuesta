<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApostadoresModel extends Model
{
    protected $table = 'apostadores';
	
    protected $fillable = [
		'nombre_apostador',
		'marcador1_apostador',
		'marcador2_apostador',
		'id_apuesta'	
		];
}
