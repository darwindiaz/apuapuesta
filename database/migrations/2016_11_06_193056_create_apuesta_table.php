<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApuestaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apuestas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_apuesta');
            $table->double('valor_apuesta',15,4);
            $table->string('aversario1_apuesta');
            $table->string('aversario2_apuesta');
            $table->string('tipo_apuesta');
            $table->date('fin_apuesta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apuestas');
    }
}
