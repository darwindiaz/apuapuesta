<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApostadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apostadores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_apostador');
            $table->string('marcador1_apostador');
            $table->string('marcador2_apostador');
            $table->integer('id_apuesta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apostadores');
    }
}
