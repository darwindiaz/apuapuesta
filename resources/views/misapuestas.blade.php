	@extends('app')

@section('htmlheader_title')
    Mis Apuestas
@endsection

@section('contentheader_title')
    Mis Apuestas
@endsection

@section('contentheader_description')
    Todas Mis Apuestas.
@endsection

@section('nivelApu')
    Mis Apuestas
@endsection

@section('main-content')
		<div class="row">
		
			<div class="pad margin no-print hidden" id="divMensaje">
		      	<div class="alert alert-success alert-dismissible" id="divAlert">
		        	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		        	<b><i class="icon fa fa-check"></i> Alerta!</b>
		        	<i id='mensaje'></i>
		  		</div>
		    </div>

			<div class="col-md-10 col-lg-offset-1">
				
				<div class="box box-solid box-info" id="panel_apostadores">
					<div class="box-header">
		              	<h3 class="box-title">Mis Apustas</h3>
		            </div>
					<div class="box-body">
						<div class="col-lg-12">
							<div class="box-body table-responsive no-padding">
								<table class="table table-hover" id="tabla_apostadores">
									<thead>
										<tr>
											<th>Nombre de la Apuesta:</th>
											<th>Valor</th>
											<th>Tipo De Apuesta</th>
											<th>Primer Aversario</th>
											<th>Primer Aversario</th>
											<th>Fecha de Terminacion</th>
											<th>Ver</th>
											<th>Eliminar</th>
											<th>Compartir</th>
										</tr>
									</thead>
									<tbody>
										 @foreach ($apuestas as $apuesta)

											<tr id='{{ $apuesta->id }}'>
												<td>{{ $apuesta->nombre_apuesta }}</td>
												<td  align="right">${{ $apuesta->valor_apuesta }}</td>
												<td>{{ $apuesta->tipo_apuesta }}</td>
												<td>{{ $apuesta->aversario1_apuesta }}</td>
												<td>{{ $apuesta->aversario2_apuesta }}</td>
												<td align="center">{{ $apuesta->fin_apuesta }}</td>
												<td align="center">
													
													<a href="{{ route('apuesta.edit',$apuesta->id) }}" class='btn btn-sm btn-social-icon editarApuesta' style='color: #00a65a'><i class='fa  fa-eye'></i></a>
												</td>
												<td class='eliminar_apuesta' data-id='{{ $apuesta->id }}'>
											 		<a class='btn btn-sm btn-social-icon' style='color: #dd4b39'>
											 			<i class='fa  fa-close'></i>
											 		</a>
											 	</td>
												<td align="center">
													
													<a href="{{ route('apostadores.show',$apuesta->id) }}" class='btn btn-sm btn-social-icon editarApuesta' style='color: #337ab7'><i class='fa fa-facebook'></i></a>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

		<!-- token -->
		<meta name="_token" content="{!! csrf_token() !!}" />

		<script type="text/javascript">
			$(document).ready(function() {
				function eliminarApuesta(){
			    	var formData = {
						id: $(this).data('id')
			        }

			        $.ajax({
						type: 'DELETE',
			            headers: {
			                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			            },
			            url: '/apuesta/'+formData.id,
			            dataType: 'json',
			            success: function(data){
			            	console.log(data);
			            	$("#"+formData.id).remove();

			            	$("#divAlert").removeClass('alert-success');
			            	$("#divAlert").addClass('alert-danger');
			               	$("#divMensaje").removeClass('hidden');
			               	$("#divMensaje").removeClass('animated pulse');
			               	$("#divMensaje").addClass('animated pulse');
			               	$("#mensaje").text('Se ha Eliminado Su Apuesta.');
			            	
			            },
			            error: function(data){
			            	console.log('Error Eliminar');
			            }
					});

			    }

	    		$(".eliminar_apuesta").click(eliminarApuesta);
			});
		</script>
@endsection


