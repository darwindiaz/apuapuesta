@extends('app')

@section('htmlheader_title')
    Patrocinadores
@endsection

@section('contentheader_title')
    Patrocinadores
@endsection


@section('main-content')
	<div class="row">
		<div class="col-md-5 col-md-offset-1">
			<div class="panel panel-primary">
				<div class="panel-heading">La Fotocopias más Veloz</div>

				<div class="panel-body">
					<img src="{{asset('/img/fotocopia.jpeg')}}" class="img-responsive img-thumbnail">
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="panel panel-primary">
				<div class="panel-heading">La Mejor Hamburguesas de la U</div>

				<div class="panel-body">
				<img src="{{asset('/img/cofeteria.jpeg')}}" class="img-responsive img-thumbnail">
				</div>
			</div>
		</div>
	</div><div class="row">
		<div class="col-md-5 col-md-offset-1">
			<div class="panel panel-primary">
				<div class="panel-heading">Desayuna sano y Saludable</div>

				<div class="panel-body">
					<img src="{{asset('/img/cereal.jpeg')}}" class="img-responsive img-thumbnail">
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="panel panel-primary">
				<div class="panel-heading">Lo mejor despues de un Duro Parcial</div>

				<div class="panel-body">
				<img src="{{asset('/img/girasoles.jpeg')}}" class="img-responsive img-thumbnail">
				</div>
			</div>
		</div>
	</div>
@endsection
