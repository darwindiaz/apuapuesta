@extends('app')

@section('htmlheader_title')
    Nueva Apuesta
@endsection

@section('contentheader_title')
    Nueva Apuesta
@endsection

@section('contentheader_description')
    Editar Mi Apuesta
@endsection

@section('nivelApu')
    Mis Apustas > Editar
@endsection

@section('main-content')
	<div class="row">

		<div class="pad margin no-print hidden" id="divMensaje">
	      	<div class="alert alert-success alert-dismissible" id="divAlert">
            	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            	<b><i class="icon fa fa-check"></i> Alerta!</b>
            	<i id='mensaje'></i>
      		</div>
	    </div>
        <div class="modal inmodal" id="modalAtualizar" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class= animated flipInY">
                    <div class="box box-info animated flipInY">
						<div class="box-header with-border">
			              	<h3 class="box-title">Actualizar Datos del Appostador.</h3>
			            </div>
						<div class="box-body" style="background-color: aliceblue;">		   
							<div class="form-group col-lg-12" style='padding-left: 0px;'>
								<input type="hidden" name="actualizar_id" id="actualizar_id">
								{!! Form::label('actualizar_nombre_apostador', 'Nombre del Apostador', ['class' => 'awesome']) !!}
								{!! Form::text('actualizar_nombre_apostador', null, ['class'=>'form-control', 'placeholder'=>"Coloca Nombre Apostador"]) !!}
							</div>
							<div class="form-group col-lg-6" style='padding-left: 0px;'>
								{!! Form::label('actualizar_marcador1_apostador', 'Marcador Adv. 1:', ['class' => 'awesome']) !!}
								{!! Form::number('actualizar_marcador1_apostador', null, ['class'=>'form-control','placeholder'=>"0"]) !!}
							</div>
							<div class="form-group col-lg-6" style='padding-left: 0px;'>
								{!! Form::label('actualizar_marcador2_apostador', 'Marcador Adv. 2:', ['class' => 'awesome']) !!}
								{!! Form::number('actualizar_marcador2_apostador', null, ['class'=>'form-control','placeholder'=>"0"]) !!}
							</div>
						</div>
						<div class="box-footer">
							<button class="btn btn-info pull-right" id="actualizar_apostador" data-dismiss="modal">Actualizar</button>
							<button class="btn btn-danger pull-right" data-dismiss="modal" style="margin-right: 5px;">Salir</button>
						</div>
					</div> 
                </div>
            </div>
        </div>


		<div class="col-md-10  col-lg-offset-1">
			<div class="box box-solid box-primary">
				<div class="box-header">
					<label class="center">Nueva Apuesta</label>
				</div>

				<div class="box-body">
					<div class="col-lg-12">
						
						<!-- token -->
						<meta name="_token" content="{!! csrf_token() !!}" />

					    <div class="form-group col-lg-5" style='padding-left: 0px;'>
							{!! Form::label('nombre_apuesta', 'Nombra Tu Apuesta:', ['class' => 'awesome']) !!}
							{!! Form::text('nombre_apuesta', $apuesta->nombre_apuesta, ['class'=>'form-control', 'placeholder'=>"Ejm: Petaco del Viernes"]) !!}
						</div>
					    <div class="form-group col-lg-4" style='padding-left: 0px;'>
							{!! Form::label('valor_apuesta', 'Valor:', ['class' => 'awesome']) !!}
							{!! Form::number('valor_apuesta', $apuesta->valor_apuesta, ['class'=>'form-control']) !!}
						</div>	    
						<div class="form-group col-lg-3" style='padding-left: 0px;'>
							{!! Form::label('tipo_apuesta', 'Tipo De Apuesta', ['class' => 'awesome']) !!}
							{!! Form::select('tipo_apuesta', ['Comida' => 'Comida', 'Dinero' => 'Dinero', 'Bebidas' => 'Bebidas', 'Salida' => 'Salida', 'Otro' => 'Otro'], $apuesta->tipo_apuesta, ['class'=>'form-control','placeholder' => 'Selecciona tipo de premio']) !!}
						</div>
						 <div class="form-group col-lg-6" style='padding-left: 0px;'>
							{!! Form::label('aversario1_apuesta', 'Primer Aversario', ['class' => 'awesome']) !!}
							{!! Form::text('aversario1_apuesta', $apuesta->aversario1_apuesta, ['class'=>'form-control', 'placeholder'=>"Ingrese nombre del 1° adversario"]) !!}
						</div>
						<div class="form-group col-lg-6" style='padding-left: 0px;'>
							{!! Form::label('aversario2_apuesta', 'Segundo Aversario', ['class' => 'awesome']) !!}
							{!! Form::text('aversario2_apuesta', $apuesta->aversario2_apuesta, ['class'=>'form-control', 'placeholder'=>"Ingrese nombre del 2° adversario"]) !!}
						</div>	
						<div class="form-group col-lg-3" style='padding-left: 0px;'>
							{!! Form::label('fin_apuesta', 'Fecha de Terminacion', ['class' => 'awesome']) !!}
							{!! Form::date('fin_apuesta',$apuesta->fin_apuesta,['class'=>'form-control'])!!}
						</div>

						<div class="form-group col-lg-2" style='padding-left: 0px;'>
							<button class="btn btn-success" style="margin-top: 23px;" id="actualizar_apuesta">Actualizar</button>
						</div>
					</div>


					<input type="hidden" id="id_apuesta" value="{{ $apuesta->id }}">
					
					
					<div class="col-lg-12 ">					   
						<div class="form-group col-lg-5" style='padding-left: 0px;'>
							{!! Form::label('nombre_apostador', 'Nombre del Apostador', ['class' => 'awesome']) !!}
							{!! Form::text('nombre_apostador', null, ['class'=>'form-control', 'placeholder'=>"Coloca Nombre Apostador"]) !!}
						</div>
						<div class="form-group col-lg-2" style='padding-left: 0px;'>
							{!! Form::label('marcador1_apostador', 'Marcador Adv. 1:', ['class' => 'awesome']) !!}
							{!! Form::number('marcador1_apostador', null, ['class'=>'form-control','placeholder'=>"0"]) !!}
						</div>
						<div class="form-group col-lg-2" style='padding-left: 0px;'>
							{!! Form::label('marcador2_apostador', 'Marcador Adv. 2:', ['class' => 'awesome']) !!}
							{!! Form::number('marcador2_apostador', null, ['class'=>'form-control','placeholder'=>"0"]) !!}
						</div>
						<div class="form-group col-lg-3" style='padding-left: 0px;'>
							<button class="btn btn-success" style="margin-top: 23px;" id="agregar_apostador">Agregar</button>
						</div>
					</div>

					
				</div>
			</div>
			@if(count($apostadores)>0)
				<div class="box box-solid box-info" id="panel_apostadores">
			@else
				<div class="box box-solid box-info hidden" id="panel_apostadores">
			@endif
				<div class="box-header">
	              	<h3 class="box-title">Lista de Apostadores</h3>
	            </div>
				<div class="box-body">
					<div class="col-lg-12">
						<div class="box-body table-responsive no-padding">
							<table class="table table-hover" id="tabla_apostadores">
								<thead>
									<tr>
										<th>Nombre Apostador</th>
										<th colspan="2">Marcador</th>
										<th colspan="2">Operaciones</th>
									</tr>
								</thead>
								<tbody>
									
									 @foreach ($apostadores as $apostador)
									 	<tr id='{{ $apostador->id }}'>
										 	<td id='nombre_apostador{{ $apostador->id }}'>{{ $apostador->nombre_apostador }}</td>
										 	<td id='marcador1_apostador{{ $apostador->id }}'>{{ $apostador->marcador1_apostador }}</td>
										 	<td id='marcador2_apostador{{ $apostador->id }}'>{{ $apostador->marcador2_apostador }}</td>
										 	
										 	<td class='editar_apostador' data-id='{{ $apostador->id }}'>
										 		<a class='btn btn-sm btn-social-icon' style='color: #00a65a' data-toggle='modal' data-target='#modalAtualizar'>
										 			<i class='fa  fa-hand-o-up'></i>
									 			</a>
								 			</td>
										 	<td class='eliminar_apostador' data-id='{{ $apostador->id }}'>
										 		<a class='btn btn-sm btn-social-icon' style='color: #dd4b39'>
										 			<i class='fa  fa-close'></i>
										 		</a>
										 	</td>
									 	</tr>
									 @endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<script type="text/javascript">
	$(document).ready(function() {


	    /**
		##################################################
							Actualizar Apuesta
		##################################################
		*/
	    function actualizarApuesta(){
	    	var id_apuesta = $("#id_apuesta").val();
	        var formData = {
				nombre_apuesta: $("#nombre_apuesta").val(),
				valor_apuesta: $("#valor_apuesta").val(),
				aversario1_apuesta: $("#aversario1_apuesta").val(),
				aversario2_apuesta: $("#aversario2_apuesta").val(),
				tipo_apuesta: $("#tipo_apuesta").val(),
				fin_apuesta: $("#fin_apuesta").val()
	        }
	        if (formData.nombre_apuesta!=''&&formData.valor_apuesta!=''&&formData.aversario1_apuesta!=''&&formData.aversario2_apuesta!=''&&formData.tipo_apuesta!=''&&formData.fin_apuesta!=''){

		        $.ajax({
		            type: "PUT",
		            headers: {
		                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
		            },
		            url: '/apuesta/'+id_apuesta,
		            dataType: 'json',
		            data: formData,
		            success: function (data) {
		               console.log('Bien:', data);

		            	$("#divAlert").removeClass('alert-danger');
		            	$("#divAlert").addClass('alert-success');
		               	$("#divMensaje").removeClass('hidden');
		               	$("#divMensaje").removeClass('animated pulse');
		               	$("#divMensaje").addClass('animated pulse');
		               	$("#mensaje").text('Se ha Actualizado Su Apuesta.');
		            },
		            error: function (data) {
		                console.log('Error:', data);
		            }
		        });
		    }else{
            	$("#divAlert").removeClass('alert-success');
		    	$("#divAlert").addClass('alert-danger');
               	$("#divMensaje").removeClass('hidden');
               	$("#divMensaje").removeClass('animated pulse');
               	$("#divMensaje").addClass('animated pulse');
               	$("#mensaje").text('Es Necesario Todos lo Datos.');
		    }
	    }

	    /**
		##################################################
							Crear Apostador
		##################################################
		*/
	    function nuevoApostador(){
	    	$('#panel_apostadores').removeClass('hidden');
	        var formData = {
				id_apuesta: $("#id_apuesta").val(),
				nombre_apostador: $("#nombre_apostador").val(),
				marcador1_apostador: $("#marcador1_apostador").val(),
				marcador2_apostador: $("#marcador2_apostador").val()
	        }
	        
	        $.ajax({
	            type: "POST",
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	            },
	            url: '/apostadores',
	            dataType: 'json',
	            data: formData,
	            success: function (data) {
	               console.log('Bien:', data);
                 	
                 	var nuevaFila = '<tr id='+data.id+'><td>'+data.nombre_apostador+'</td>';
					nuevaFila += '<td>'+data.marcador1_apostador+'</td>';
					nuevaFila += '<td>'+data.marcador2_apostador+'</td>';

					nuevaFila += "<td class='editar_apostador' data-id="+data.id+"><a class='btn btn-sm btn-social-icon' style='color: #00a65a' data-toggle='modal' data-target='#modalAtualizar'><i class='fa  fa-hand-o-up'></i></a></td>";

	                nuevaFila += "<td class='eliminar_apostador' data-id="+data.id+"><a class='btn btn-sm btn-social-icon' style='color: #dd4b39'><i class='fa  fa-close'></i></a></td></tr>";
	                
	                $("#tabla_apostadores").append(nuevaFila);
	                $("#nombre_apostador").val("");
	                $("#marcador1_apostador").val("");
	                $("#marcador2_apostador").val("");
	                $(".eliminar_apostador").click(eliminarApostador);
	                
	            	$("#divAlert").removeClass('alert-danger');
	            	$("#divAlert").addClass('alert-success');
	               	$("#divMensaje").removeClass('hidden');
	               	$("#divMensaje").removeClass('animated pulse');
	               	$("#divMensaje").addClass('animated pulse');
	               	$("#mensaje").text('Se ha Crado un Nuevo Apostador.');
	            },
	            error: function (data) {
	                console.log('Error:', data);
	            }
	        });
	    }
	    function eliminarApostador(){
	    	var formData = {
				id_apostador: $(this).data('id')
	        }

	        $.ajax({
				type: 'DELETE',
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	            },
	            url: '/apostadores/'+formData.id_apostador,
	            dataType: 'json',
	            success: function(data){
	            	console.log(data);
	            	$("#"+formData.id_apostador).remove();

	            	$("#divAlert").removeClass('alert-success');
	            	$("#divAlert").addClass('alert-danger');
	               	$("#divMensaje").removeClass('hidden');
	               	$("#divMensaje").removeClass('animated pulse');
	               	$("#divMensaje").addClass('animated pulse');
	               	$("#mensaje").text('Se ha Crado Su Apuesta.');
	            	
	            },
	            error: function(data){
	            	console.log('Error Eliminar');
	            }
			});

	    }

	    function editaraApostador(){
	    	var id_apuesta = $(this).data('id');
	    	$.ajax({
	            type: "GET",
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	            },
	            url: '/apostadores/'+id_apuesta+'/edit',
	            dataType: 'json',
	            success: function (data) {
	               console.log('Bien:', data);
	    			$("#actualizar_id").val(data.id);
	    			$("#actualizar_nombre_apostador").val(data.nombre_apostador);
    				$("#actualizar_marcador1_apostador").val(data.marcador1_apostador);
    				$("#actualizar_marcador2_apostador").val(data.marcador2_apostador);
	            },
	            error: function (data) {
	                console.log('Error:', data);
	            }
	        });

     	}
     	function actualizarApostador(){
     		var id_apostador = $("#actualizar_id").val();
	        var formData = {
				nombre_apostador: $("#actualizar_nombre_apostador").val(),
				marcador1_apostador: $("#actualizar_marcador1_apostador").val(),
				marcador2_apostador: $("#actualizar_marcador2_apostador").val(),
	        }
	        $.ajax({
	            type: "PUT",
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	            },
	            url: '/apostadores/'+id_apostador,
	            dataType: 'json',
	            data: formData,
	            success: function (data) {
	               console.log('Bien->:', data);
					$("#nombre_apostador"+id_apostador).text(data.nombre_apostador);
					$("#marcador1_apostador"+id_apostador).text(data.marcador1_apostador);
					$("#marcador2_apostador"+id_apostador).text(data.marcador2_apostador);
					
	            	$("#divAlert").removeClass('alert-danger');
	            	$("#divAlert").addClass('alert-success');
	               	$("#divMensaje").removeClass('hidden');
	               	$("#divMensaje").removeClass('animated pulse');
	               	$("#divMensaje").addClass('animated pulse');
	               	$("#mensaje").text('Se ha Actualizado Su Apuesta.');

	            },
	            error: function (data) {
	                console.log('Error:', data);
	            }
	        });
	    }

	    $("#actualizar_apostador").click(actualizarApostador);
	    $(".editar_apostador").click(editaraApostador);

	    $('#actualizar_apuesta').click(actualizarApuesta);
	    $("#agregar_apostador").click(nuevoApostador);
	    $(".eliminar_apostador").click(eliminarApostador);
	   
	});
</script>

@endsection


